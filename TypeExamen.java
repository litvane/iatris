/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */

    // Un acte peut-être de deux types : 
public enum TypeExamen{
        DIAGNOSTIQUE("DIAGNOSTIQUE"),
        THERAPEUTIQUE("THERAPEUTIQUE");
    
    private String nom;
    
    private TypeExamen(String nom){
        this.nom=nom;
    }
}
