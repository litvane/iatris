/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
// Chaque praticien hospitalier (classe PraticienHospitalier) a un attribut fonction qui correspond à une des fonctions suivantes :
public enum Fonction {

    
    CARDIOLOGUE("cardiologue"),
    UROLOGUE("urologue"),
    GYNECOLOGUE("gynecologue"),
    DERMATOLOGUE("dermatologue"),
    PSYCHIATRE("psychiatre"),
    RADIOLOGUE("radiologue"),
    ADDICTOLOGUE("addictologue"),
    GASTROENTEROLOGUE("gastroenterologue"),
    ANAPATHOLOGUE("anapathologue"),
    HEMATOLOGUE("hematologue"),
    ANESTHESISTE("anesthesiste"),
    LABORANTIN("laborantin"),
    CHEFDESERVICE("chef de service"),
    INTERNE("interne"),
    INFIRMIER("infirmier"),
    CHIRURGIEN("chirurgien"),
    GENETICIEN("geneticien"),
    OPHTALMOLOGUE("ophtalmologue"),
    URGENTISTE("urgentiste"),
    PEDIATRE("pediatre"),
    ONCOLOGUE("oncologue"),
    ONDONTOLOGUE("odontologue"),
    MAÏEUTICIEN("maïeuticien"),
    AMBULANCIER("ambulancier"),
    PHARMACIEN("pharmacien")
    SECRETAIRE_MEDICAL("secretaire medical"),
    SECRETAIRE_ADMINISTRATIF("secretaire administratif");
    
    
    private String fonction;
/**
 * Constructeur de Fonction
 * @param fonction represente la fonction du praticien hospitalier 
 */
    Fonction(String fonction) {
        this.fonction = fonction;
    }

/**
 * 
 * @return la fonction
 */
    public String getFonction() {
        return fonction;
    }
    

}
