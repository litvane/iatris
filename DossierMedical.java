/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
import java.util.Vector;

public class DossierMedical {

    private Vector<FicheDeSoins> fiches;     // contient des objets de classe 'FicheDeSoins'

    public DossierMedical() {
        fiches = new Vector<FicheDeSoins>();  // liste vide
    }

// Ajout d’une fiche de type FichedeSoins à la liste de FicheDeSoins 
    public void ajouterFiche(FicheDeSoins fiche) {       
fiches.add(fiche);
    }

// Affichage de toutes les Fiches de Soins du Dossier Médical 
    public void afficher() {
        System.out.println("Dossier médical informatisé :");
        System.out.println("-----------------------------");
        for (int i = 0; i < fiches.size(); i++) {
            FicheDeSoins f = fiches.get(i);
            f.afficher();
            // pour séparer les fiches de soins :
            System.out.println("--------------------------------------");
        }
    }



// Tri des fiches par dates 
    public void trierDates() {
        Vector<FicheDeSoins> copieFiches = new Vector<FicheDeSoins>(fiches);

        while (!copieFiches.isEmpty()) {
            // on cherche la fiche de soins de date minimale :
            int imin = 0;
            FicheDeSoins f1 = copieFiches.get(imin);
            for (int i = 1; i < copieFiches.size(); i++) {
                FicheDeSoins f2 = copieFiches.get(i);
                if (f2.getDate().compareTo(f1.getDate()) < 0) {
                    imin = i;
                    f1 = f2;
                }
            }
            // on affiche la fiche de soins trouvée
            f1.afficher();
            System.out.println("------------------------");
            //on supprime de la liste la plus ancienne fiche)  afin de faire une nouvelle recherche dans la liste,
            copieFiches.remove(imin);  
        }
    }
   
}
