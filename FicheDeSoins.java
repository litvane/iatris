/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
import java.util.Vector;

public class FicheDeSoins {
    private Patient patient;
    private PraticienHospitalier praticienHospitalier;
    private Date date;
    private Vector<Examen> examens;       // contient des objets de classe 'Examen'
    
    public FicheDeSoins(Patient patient, PraticienHospitalier praticienHospitalier, Date date) {
        this.patient = patient;
        this.praticienHospitalier = praticienHospitalier;
        this.date = date;
        examens = new Vector<Examen>();   // liste vide
        }
    
    public Patient getPatient() { return patient; }
    public PraticienHospitalier getPraticienHospitalier() { return praticienHospitalier; }
    public Date    getDate()    { return date; }
    public Vector<Examen> getExamen () {return examens;}
    
    public void ajouterExamen(Examen examen) {
        examens.add(examen);
        }
    
    public void supprimerActe(Examen examen){
        examens.remove(examen); 
    }
    
    public void ajouterExamen(int numArchives, String nom,Patient patient, String observationPraticienHospitalier, TypeExamen type, Date date) { //création
        Examen examen = new Examen (numArchives, nom, patient, observationPraticienHospitalier, type, date);
        examens.add(examen);
        }
    
    public void afficher() {
        System.out.println("Fiche de soins du " + date.toString());
        System.out.println("- praticien hospitalier : " + praticienHospitalier.toString());
        System.out.println("- patient : " + patient.toString());
        System.out.println("- actes medicaux :");
        for (int i=0; i<examens.size(); i++) {
            Examen a = examens.get(i);
            System.out.println("    > " + a.toString());
            }
        }
    
    }

