/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import BD.*;

import interfaces.InterfaceConnexion;
import interfaces.SecretaireMedicale;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 *
 * @author Elisa
 */

//En tant que praticien hospitalier
//Je veux visualiser les examens 
//Dans le but d’avoir une information sur un patient en particulier

//On part du principe que le praticien hospitalier est déjà connecté à son compte 
public class AccesDMA {
    private String nomUsuelPatient;
    private String nomNaissancePatient;
    private String prenomPatient;
    private Date dateNaissance;
    private String service;
    private int numeroSejour;
    private Date date;
    private String nomPHrespo;
    private String lettrePHrespo;
    private String natureMedicoTech;
    //private static final String configurationFile = "BD.properties";
    private String nom;
    private String prenom;
    private String fonction;
    private String login;
    private String mdp;
    
    
    public AccesDMA (String nomUsuelPatient, String nomNaissancePatient, String prenomPatient, Date dateNaissance,String service, int numeroSejour,Date date,String nomPHrespo,String lettrePHrespo, String natureMedicoTech) {
        this.nomUsuelPatient=nomUsuelPatient;
        this.nomNaissancePatient=nomNaissancePatient;
        this.prenomPatient=prenomPatient;
        this.dateNaissance=dateNaissance;
        this.service=service;
        this.numeroSejour=numeroSejour;
        this.date=date;
        this.nomPHrespo=nomPHrespo;
        this.lettrePHrespo=lettrePHrespo;
        this.natureMedicoTech=natureMedicoTech;
        
        

        }
    public void connexion(int ipp, String service) {
        try {
            //DatabaseAccessProperties dap = new DatabaseAccessProperties();
            String jdbcDriver = "oracle.jdbc.driver.OracleDriver";
            String dbUrl = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
            String username = "samsong";
            String password = "a12cfc98c8";

            Connection con = DriverManager.getConnection(dbUrl, username, password);

            String query = "SELECT nomUsuelPatient, nomNaissancePatient, prenomPatient, dateNaissance, service, numeroSejour, date, nomPHrespo, lettrePHrespo, natureMedicoTech FROM DMA";
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery(query);
            
}
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        System.out.println("Le patient  : " + nomUsuelPatient + " "+ nomNaissancePatient+ " né(e) le "+ dateNaissance +"dont le numéro de séjour est le suivant" + numeroSejour);
        System.out.println("a été admis dans le service suivant " + service +"pour des examens de nature" + natureMedicoTech);
        System.out.println("le " + date);
        System.out.println("Par le Praticien Hospitalier responsable Dr " + nomPHrespo);
        System.out.println("Il a rédigé la lettre suivante " + lettrePHrespo);
        
        
        
    }
}
