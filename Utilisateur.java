/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
public class Utilisateur {
    
    String nom;
    String prenom;
    String mdp; 
    String id; 
    
    // permet de définir les mots de passe, identifiants et métiers du personnel (medecin et secretaire administrative et medicale)
    public Utilisateur (String nom, String prenom, String id, String mdp) {
        this.nom = nom;
        this.prenom = prenom;
        this.id=id;
        this.mdp=mdp; 
    }
    //permet de recuperer la chaine de caractère representant le metier de chaque personnel  de l’hopital en fonction de l’ instance des objets
    public String getMetier (){
        if(this instanceof PraticienHospitalier){
            return "PRATICIEN HOSPITALIER";
        }
        else if (this instanceof SecretaireAdministratif){
            return "SECRETAIRE ADMINISTRATIF";
        }
        else {
            return "SECRETAIRE MEDICALE";
        }
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMdp() {
        return mdp;
    }

    public String getId() {
        return id;
    }

    
}


