/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
import java.sql.Date;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

public class Patient {
    private int numeroSejour;
    private String nom;
    private String prenom;
    private String ipp; 
    private Date dateNaissance;
    private String genre;
    private String adresse;
    private String nom_praticien_responsable;
    private String prenom_praticien_responsable;
    
    
    public Patient(int numeroSejour, String nom, String prenom,String ipp, Date dateNaissance, String genre, String adresse, String nom_praticien_responsable, String prenom_praticien_responsable) {
        this.numeroSejour=numeroSejour;
        this.nom = nom;
        this.prenom = prenom;
        this.ipp= ipp; 
        this.dateNaissance = dateNaissance;
        this.genre=genre;
        this.adresse = adresse;
        this.nom_praticien_responsable=nom_praticien_responsable;
        this.prenom_praticien_responsable=prenom_praticien_responsable;
        }

    public int getNumeroSejour() {
        return numeroSejour;
    }
    
    public void setNumeroSejour(int numeroSejour){
        this.numeroSejour=numeroSejour;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getIpp() {
        return ipp;
    }

    public void setIpp(String ipp) {
        this.ipp = ipp;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    
    public String getGenre() {
        return genre;
    }
    
    public void setGenre(String genre){
        this.genre=genre;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    public String getNom_praticien_responsable() {
        return nom_praticien_responsable;
    }

    public void setNom_praticien_responsable(String nom_praticien_responsable) {
        this.nom_praticien_responsable = nom_praticien_responsable;
    }
    
    public String getPrenom_praticien_responsable() {
        return prenom_praticien_responsable;
    }

    public void setPrenom_praticien_responsable(String prenom_praticien_responsable) {
        this.nom_praticien_responsable = prenom_praticien_responsable;
    }
    
    public String toString() {
        return prenom + " " + nom+ ", n° IPP : "+ ipp + ", Né(e) le : " + dateNaissance + ", vit au : "+ adresse;
        }
    
    public boolean equals(Object o) {
        if (o instanceof Patient) {
            Patient p = (Patient)o;
            return nom.equals(p.nom) && prenom.equals(p.prenom) && ipp.equals(p.ipp);
            }
        else
            return false;
        } 
    
    
    }
 

