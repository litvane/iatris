package iatris;

import iatris.DatabaseAccessProperties;
import iatris.SQLWarningsExceptions;
import iatris.bdmetier;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Connexion {

    private String nom;
    private String prenom;
    private String fonction;
    private String login;
    private String mdp;

    public Connexion(String nom, String prenom, String fonction) { //fonction : si c'est un médecin, un infirmier,...
        this.nom = nom;
        this.prenom = prenom;
        this.fonction = fonction;
        login = "";
        mdp = "";
    }

    public Connexion(String nom, String prenom, String fonction, String login, String mdp) {//nouveau constructeur avec les attributs ajoutés
        this.nom = nom;
        this.prenom = prenom;
        this.fonction = fonction;
        this.login = login;
        this.mdp = mdp;
    }

    public String getNom() {
        return nom;
    }  //renvoie le nom du médecin

    public String getPrenom() {
        return prenom;
    }  //renvoie son prénom

    public String getFonction() {
        return fonction;
    } //renvoie la fonction

    public String getLogin() {
        if (login != "") {
            return login;
        } else {
            String s = "Pas de login";
            return s;
        }
    } //renvoie son login

    public String getMdp() {
        if (mdp != "") {
            return mdp;
        } else {
            String s = "Pas de mot de passe";
            return s;
        }
    } //renvoie son mdp

    public String toString() {  //converti en une String
        String s;
        s = "Dr " + prenom + " " + nom + ", " + fonction;
        return s;
    }

    public boolean verifLogin(String pseudo) { //vérifie si le login est bien celui d'un médecin
        if (pseudo.equals(login)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verifMdp(String pseudo, String mot) {  //vérifie si le mot de passe correspond bien au login
        if (verifLogin(pseudo)) {
            if (mot.equals(mdp)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String afficheResultatMdp(String pseudo, String mot) {  //affice si le mot de passe n'est pas le bon
        String s = "";
        if (verifMdp(pseudo, mot)) {
            s = "Identification réussie";
        } else {
            s = "Identification échouée, veuillez réessayer";
        }
        System.out.println(s);
        return s;
    }

    public boolean equals(Object o) {   //Permet de vérifier s'il s'agit bien du même médecin (même les homonymes peuvent être différenciés grâce à la spécialité)
        if (o instanceof Connexion) {
            Connexion p = (Connexion) o;
            return nom.equals(p.nom) && prenom.equals(p.prenom) && fonction.equals(p.fonction);
        } else {
            return false;
        }
    }

    String getText() {
       return login; //To change body of generated methods, choose Tools | Templates.
    }
}
