create table FONCTION(
    fonction VARCHAR(30) UNIQUE
);

create table SPECIALITE(
    specialite VARCHAR(30)
);

create table METIER(
    nom CHAR(30) UNIQUE NOT NULL, 
    prenom CHAR(30) UNIQUE NOT NULL,
    fonction VARCHAR(30) references FONCTION(fonction),
    specialite VARCHAR(30), 
    telephone CHAR(10),
    id CHAR(30) UNIQUE NOT NULL,
    mdp CHAR(30) NOT NULL,
    PRIMARY KEY ( id, mdp )
);

//peut ne pas avoir de numéro de séjour si patient pas dans lhôpital en ce moment 
create table PATIENT                     
    (numeroSejour INTEGER,
    nom CHAR(30) NOT NULL,         
    nomNaissance CHAR(30),
	prenom CHAR(30) NOT NULL,
	ipp INTEGER NOT NULL,           
	dateNaissance DATE NOT NULL,             
    genre CHAR(1) CHECK (genre='H' OR genre='F'),
	adresse VARCHAR(100),
    nom_praticien_responsable CHAR(30) references METIER(nom),           
	prenom_praticien_responsable CHAR(30) references METIER(prenom), 
    PRIMARY KEY ( ipp )        
);


create table ACTE(                          
    nom CHAR(30) NOT NULL,                          
    prenom CHAR(30) NOT NULL,
    dateNaissance DATE NOT NULL,
    nom_praticien CHAR(30) NOT NULL references METIER(nom),
    prenom_praticien CHAR(30) NOT NULL references METIER(prenom),
    nom_acte CHAR(30),
    dateActe DATE NOT NULL
);


create table SERVICE(
    service VARCHAR(30) UNIQUE
);


//mettre le nom du patient dans la table aussi ?
create table EXAMEN(            
    numArchives INTEGER NOT NULL,
    dateExam DATE NOT NULL, 
    nom CHAR(30) NOT NULL,                          
    prenom CHAR(30) NOT NULL,
    dateNaissance DATE NOT NULL,
    service VARCHAR(30) references SERVICE(service),                       
    typeExamen VARCHAR(30) CHECK (typeExamen='diagnostique' OR typeExamen='thérapeutique'),
    typeImagerie VARCHAR(30),
    nom_praticien CHAR(30) NOT NULL references METIER(nom),
    prenom_praticien CHAR(30) NOT NULL references METIER(prenom),                         
    resultat VARCHAR(100) NOT NULL,                  
    observations VARCHAR(200)
);

--> pb avec datetime, donc marche pas pour l'instant
create table CONSULTATIONS(
    date_heure_consultation Datetime NOT NULL,  
    service VARCHAR(30) references SERVICE(service),
    nom_praticien CHAR(30) NOT NULL references METIER(nom),
    prenom_praticien CHAR(30) NOT NULL references METIER(prenom),
    observations VARCHAR(200)
);


// numeroSejour INTEGER ?
create table DM( 
    service CHAR(30) NOT NULL #CHECK (service='clinique' OR service='medico-technique'),
    date_creation CHAR(10) NOT NULL,
    idCreateur CHAR(30) NOT NULL #references METIER(id),
    nom_patient CHAR(30) NOT NULL,                          
    prenom_patient CHAR(30) NOT NULL,
    dateNaissance CHAR(10) NOT NULL,
    raisonAdmission CHAR(30),
    resultat VARCHAR(100),
    observationsPH VARCHAR(200),
    prescriptionsPH VARCHAR(200),
    operationsIFSI VARCHAR(200),
    natureMedicoTech VARCHAR(100),
    resultatsMedicoTech VARCHAR(100),
    nom_praticien_responsable CHAR(30) #references METIER(nom),           
    lettrePHrespo VARCHAR(200)
);

remettre la contrainte plus tard 
create table DMA( 
    nom_patient CHAR(30),
    nomNaissance_patient CHAR(30),
    prenom_patient CHAR(30),
    dateNaissance CHAR(10) NOT NULL,
    ipp INTEGER NOT NULL #references PATIENT(ipp),
    numeroSejour INTEGER,
    date_creation CHAR(10) NOT NULL,
    nom_praticien_responsable CHAR(30),
    service VARCHAR(30),
    nature VARCHAR(30) NOT NULL #CHECK (nature="clinique" OR nature="medico-technique"),  
    lettrePHrespo VARCHAR(200)
);

insert into DMA values('LITVAN', 'LILI', 'Elisa', '1999-12-29',194594857,191284939,'2022-02-18','STARK','CARDIOLOGIE','clinique',null);


insert into SERVICE values ('CARDIOLOGIE');

insert into FONCTION values ('SECRETAIRE_MEDICAL');
insert into FONCTION values ('SECRETAIRE_ADMINISTRATIF');
insert into FONCTION values ('PRATICIEN_HOSPI_CLINIQUE');
insert into FONCTION values ('PRATICIEN_HOSPI_MEDICO_TECH');

insert into SPECIALITE values ('SECRETAIRE_MEDICAL');
insert into SPECIALITE values ('SECRETAIRE_ADMINISTRATIF');
insert into SPECIALITE values ('CARDIOLOGUE');
insert into SPECIALITE values ('UROLOGUE');
insert into SPECIALITE values ('GYNECOLOGUE');
insert into SPECIALITE values ('DERMATOLOGUE');
insert into SPECIALITE values ('PSYCHIATRE');
insert into SPECIALITE values ('RADIOLOGUE');
insert into SPECIALITE values ('ADDICTOLOGUE');
insert into SPECIALITE values ('ASSISTANT_DENTAIRE');
insert into SPECIALITE values ('GASTROENTEROLOGUE');
insert into SPECIALITE values ('ANAPATHOLOGUE');
insert into SPECIALITE values ('HEMATOLOGUE');
insert into SPECIALITE values ('ANESTHESISTE');
insert into SPECIALITE values ('CHEFDESERVICE');
insert into SPECIALITE values ('INTERNE');
insert into SPECIALITE values ('INFIRMIER');
insert into SPECIALITE values ('CHIRURGIEN');
insert into SPECIALITE values ('GENETICIEN');
insert into SPECIALITE values ('OPHTALMOLOGUE');
insert into SPECIALITE values ('URGENTISTE');
insert into SPECIALITE values ('PEDIATRE');
insert into SPECIALITE values ('ONCOLOGUE');
insert into SPECIALITE values ('ONDONTOLOGUE');
insert into SPECIALITE values ('MAÏEUTICIEN');
insert into SPECIALITE values ('AMBULANCIER');

insert into SPECIALITE values ('LABORANTIN');
insert into SPECIALITE values ('PHARMACIEN');


insert into METIER values('FERRY', 'Karine', 'SECRETAIRE_MEDICAL', 'SECRETAIRE_MEDICAL','0233000000', 'karine', 'smatique');
insert into METIER values('ALIAGAS', 'Nikos', 'SECRETAIRE_MEDICAL', 'SECRETAIRE_MEDICAL','0233000001', 'nikoslebg', 'thisisthevoice');
insert into METIER values('DRUCKER', 'Michel', 'SECRETAIRE_MEDICAL', 'SECRETAIRE_MEDICAL','0233000002', 'drudru', 'vivementdimanche');
insert into METIER values('DELAHOUSE', 'Laurent', 'SECRETAIRE_MEDICAL', 'SECRETAIRE_MEDICAL','0233000003', 'house', 'jt20h');
insert into METIER values('PERNAULT', 'Jean-Pierre', 'SECRETAIRE_MEDICAL','SECRETAIRE_MEDICAL', '0233000004', 'jp', 'jt13h');
insert into METIER values ('MAE','Christophe','SECRETAIRE_ADMINISTRATIF','SECRETAIRE_ADMINISTRATIF','0233000005', 'chrichri','cafaitmal');
insert into METIER values ('AZNAVOUR','Charles','SECRETAIRE_ADMINISTRATIF','SECRETAIRE_ADMINISTRATIF','0233000006', 'leking','laboheme');
insert into METIER values ('SANSON','Véronique','SECRETAIRE_ADMINISTRATIF','SECRETAIRE_ADMINISTRATIF','0233000007','vero','droledevie');
insert into METIER values ('MATHY','Mimie','SECRETAIRE_ADMINISTRATIF','SECRETAIRE_ADMINISTRATIF','0233000008','josephine','angegardien');
insert into METIER values ('LEROY','Nolwenn','SECRETAIRE_ADMINISTRATIF','SECRETAIRE_ADMINISTRATIF','0233000009','nolwenn','labretagne'); 
insert into METIER  values('STARK','Tony','PRATICIEN_HOSPI_CLINIQUE', 'CARDIOLOGUE','061234567','ironcardio','Ultron');
Insert into METIER  values('LIOTTA', 'Renaud','PRATICIEN_HOSPI_CLINIQUE','ADDICTOLOGUE','06695735','lexis','cubain45');
Insert into METIER  values('BONNEY-EYMMARD','Martin','PRATICIEN_HOSPI_CLINIQUE','PSYCHIATRE','0675843752','Naza67','dansedumatin');
Insert into METIER  values('SIMPSON','Homer','PRATICIEN_HOSPI_CLINIQUE','INFIRMIER','094387692','ohundonut','doh1664');
Insert into METIER  values('ATTIA','Axel','PRATICIEN_HOSPI_CLINIQUE','GASTROENTEROLOGUE','067827648','iamlate','iamhere');
Insert into METIER  values( 'HARRY','COVER','PRATICIEN_HOSPI_CLINIQUE','RADIOLOGUE' ,'06893642','yourax','lepetitlepetit');
Insert into METIER  values( 'DIAMAR','AUCANNARD','PRATICIEN_HOSPI_CLINIQUE','ASSISTANT_DENTAIRE' ,'064884653','quepqssq','gdgdgdou' );
Insert into METIER  values( 'LEFUITEUR','GINDALF','PRATICIEN_HOSPI_CLINIQUE','ANESTHESISTE' ,'06783264','ketamine34','lewee34');
Insert into METIER  values( 'WAZE','LYDIA','PRATICIEN_HOSPI_CLINIQUE','CHEFDESERVICE','0678432642','googlemaps','locaintrouvable67' );
Insert into METIER  values( 'ELMALEH', 'GAD', 'PRATICIEN_HOSPI_CLINIQUE','UROLOGUE' ,'067843264','gadouh','MDR2345' );

insert into PATIENT values (220100345,'SAMSON','ALIBERT','Gabrielle',210968547,TO_DATE('2000-07-31','YYYY-MM-DD'),'F','162 Avenue Avenue Ambroise Croizat 38400 SAINT MARTIN D HERES', 'STARK', 'Tony');
insert into PATIENT values (191284939,'LITVAN','LILI', null,'Elisa',194594857,TO_DATE('1999-12-29','YYYY-MM-DD'),'F','11 rue Elie Cartan 38100 GRENOBLE', 'STARK', 'Tony');
insert into PATIENT values (181284939, 'BAKKOU',null,'Ilias',183648293,TO_DATE('2000-05-29','YYYY-MM-DD'),'H','119 cours Jean Jores 38100 GRENOBLE', 'STARK', 'Tony');
insert into PATIENT values (211284942, 'THEFEELING',null,'Agathe',2138495381,TO_DATE('1976-01-12','YYYY-MM-DD'),'F','13 Avenue du soleil 50100 CHERBOURG', 'STARK', 'Tony');
insert into PATIENT values (191254939,'TITUDE',null,'Hatice',120946871,TO_DATE('1998-11-09','YYYY-MM-DD'),'F','67 rue des TIS 38400 SAINT MARTIN D HERES', 'STARK', 'Tony');

insert into DM values ('clinique',TO_DATE('2022-02-15','YYYY-MM-DD'), 'ironcardio', 'SAMSON', 'Gabrielle', TO_DATE('2000-07-31','YYYY-MM-DD'),  null, null, null, null, null, null, null, null);

insert into DMA values('LITVAN', 'LILI', 'Elisa', '1999-12-29',194594857,191284939,'2022-02-18','STARK','CARDIOLOGIE','clinique',null);
insert into DMA values ('SAMSON', 'Gabrielle','ALIBERT',TO_DATE('2000-07-31','YYYY-MM-DD'),210968547,null,TO_DATE('2022-03-04','YYYY-MM-DD'),'STARK','CARDIOLOGIE', 'clinique', null);

insert into EXAMEN values(123456, '2000-07-31', 'SAMSON', 'Gabrielle', '2000-07-31', 'CARDIOLOGIE', 'diagnostique', 'Echographie', 'STARK', 'Tony', 'résultat', null );
insert into EXAMEN values(654321, '2000-07-31', 'LITVAN', 'Elisa', '2000-07-31', 'CARDIOLOGIE', 'diagnostique', 'Echographie', 'STARK', 'Tony', 'résultat', null );
