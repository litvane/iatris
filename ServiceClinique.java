/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * accueille patient pour traiter pathologie identifiée
patient adressé par un médecin généraliste
propose consultations ou hospitalisations
cas particulier : service des urgences

 * @author Elisa
 */
public class ServiceClinique {
    
    private String nom;
    private String type;//propose consultations ou hospitalisations
    private String ChefDeService;
    
    
    public ServiceClinique (String nom, String type, String urgence) {
        
        this.nom = nom;
        this.type=type;
        this.ChefDeService=ChefDeService;

        }
    
      
    
    public String toString() {
        return "Le service de  " + nom + " " + "propose des "+type + "; " +"dirigé par le Dr"+ ChefDeService;
        }
    
}
