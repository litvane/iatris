package iatris;




import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * The BenchmarkProperties class describes the properties of the benchmark to
 * run.
 */
public class DatabaseAccessProperties {

    private Properties prop = new Properties();
    private String jdbcDriver;
    private String dbUrl;
    private String username, password;

    public DatabaseAccessProperties() {
        jdbcDriver = "oracle.jdbc.driver.OracleDriver";
        dbUrl = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
        username = "samsong";
        password = "a12cfc98c8";
    }

    public String getJdbcDriver() {
        return jdbcDriver;
    }

    public String getDatabaseUrl() {
        return dbUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
