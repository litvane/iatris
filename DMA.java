/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
*/
public class DMA {
    private String service;//DMA accessible par PH QUE si patient admis dans son service
    private int numeroSejour;//format YYMMxxxxx
    private Date date;
    private String nomPHrespo;//nom du praticien hospitalier responsable
    private String lettrePHrespo;//lettre de sortie rédigée par ce PH
    private String natureMedicoTech;//Nature prestations médico-technique mais PAS leur résultats
    
    
    public DMA (String service, int numeroSejour,Date date,String nomPHrespo,String lettrePHrespo, String natureMedicoTech) {
        this.service="nom du service";//nom défini dans les classes représentant les différents services 
        this.numeroSejour=numeroSejour;
        this.date=date;
        this.nomPHrespo=nomPHrespo;
        this.lettrePHrespo=lettrePHrespo;
        this.natureMedicoTech=natureMedicoTech;
        

        }
    
      public String getService() { return service; }
    
    public String lettrePHrespo() {
        return "le Praticien Hospitalier" + nomPHrespo + " " + "du service suivant : " + service + "a été responsable" +
                "du patient identifié par le numéro de séjour suivant : "+numeroSejour+" à la date suivante"+
                date+" dont les natures des prestations médico-techniques sont les suivantes : "+natureMedicoTech;
        }
}