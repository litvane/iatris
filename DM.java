/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

import java.sql.Date;

/**
 *
 * @author Elisa
 *

 //En tant que praticien hospitalier
//Je veux visualiser les examens 
//Dans le but d’avoir une information sur un patient en particulier

//On part du principe que le praticien hospitalier est déjà connecté à son compte (accessible que par PH du service SAUF PH anesthésiste ou radiologie et PH service des urgences)
 */
public class DM {
    //pour les 2 types de services 
    private String service;//distinction entre service clinique ou médico-technique 
    private Date date;//chaque ajout daté électroniquement
    private String idCreateur;//chaque ajout est signé électroniquement (carte prof de santé)
    private String nom; //faire en sorte de dire que c'est un patient ?
    private String prenom;
    private String dateNaissance;
    private String observationsPH;
    
    //propre au service médico-technique 
    private String resultats;
    
    //propre au service clinique (pas pour le médico-technique) : 
    private String prescriptionsPH;
    private String operationsIFSI;//opérations réalisées par les infirmiers avec actes sous prescription médicale
    private String resultatsMedicoTech;//résultats des services médico-techniques pour les services cliniques
    private String lettrePHrespo;//lettre de sortie rédigée par PH responsable
    
    //éléments manquants pour la lettre de sortie
    private int numeroSejour;//format YYMMxxxxx
    private String nomPHrespo;//nom du praticien hospitalier responsable
    private String natureMedicoTech;//Nature prestations médico-technique mais PAS leur résultats
    
    public DM (String service, Date date, String idCreateur, String nom, String prenom, String dateNaissance, String resultats, 
            String observationsPH, String prescriptionsPH, String operationsIFSI, String resultatsMedicoTech, String nomPHrespo, String lettrePHrespo) {
        this.service=service;
        this.date=date;
        this.idCreateur=idCreateur;
        this.nom=nom;
        this.prenom=prenom;
        this.dateNaissance=dateNaissance;
        
        this.resultats=resultats;
        this.observationsPH=observationsPH;
        this.prescriptionsPH=prescriptionsPH;
        this.operationsIFSI=operationsIFSI;
        this.resultatsMedicoTech=resultatsMedicoTech;
        this.nomPHrespo=nomPHrespo;
        this.lettrePHrespo=lettrePHrespo;
        }
    
    public String getService() { 
        return service; 
    }
    
    public Date getDate(){
        return date;
    }
    
    public String idCreateur(){
        return idCreateur;
    }
    
    public String getNom(){
        return nom;
    }
    
    public String getPrenom(){
        return prenom;
    }
    
    public String getDateNaissance(){
        return dateNaissance;
    }
    
    public String nomPHrespo(){
        return nomPHrespo;
    }
        
    public String lettrePHrespo() {
        return "le Praticien Hospitalier" + nomPHrespo + " " + "du service suivant : " + service + "a été responsable" +
                "du patient identifié par le numéro de séjour suivant : "+numeroSejour+" à la date suivante"+
                date+" dont les natures des prestations médico-techniques sont les suivantes : "+natureMedicoTech;
        }
}
