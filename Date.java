/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
public class Date implements Comparable {
    private int jour;
    private int mois;
    private int annee;
    private int heure;
    private int minutes;
    
    public Date(int annee, int mois, int jour,int heure, int minutes) {
        this.jour = jour;
        this.mois = mois;
        this.annee = annee;
        this.heure=heure;
        this.minutes=minutes;
        }

        public Date(int annee, int mois, int jour){
        this.annee = annee;
        this.mois= mois;
        this.jour= jour;
    }

    //Permet d'écrire la date (avec l’heure) dans un format correct : jj-mm-aaaa  // hh:mm
    public String toString() {
        if (getMois() < 10 && getJour() < 10 && getHeure() < 10 && getMinutes() < 10) {
            return getAnnee() + "-0" + getMois() + "-0" + getJour() + " 0" + getHeure() + ":0" + getMinutes();
        }
        if (getMois() >= 10 && getJour() < 10 && getHeure() < 10 && getMinutes() < 10) {
            return getAnnee() + "-" + getMois() + "-0" + getJour() + " 0" + getHeure() + ":0" + getMinutes();
        }
        if (getMois() >= 10 && getJour() >= 10 && getHeure() < 10 && getMinutes() < 10) {
            return getAnnee() + "-" + getMois() + "-" + getJour() + " 0" + getHeure() + ":0" + getMinutes();
        }
        if (getMois() >= 10 && getJour() >= 10 && getHeure() >= 10 && getMinutes() < 10) {
            return getAnnee() + "-" + getMois() + "-" + getJour() + " " + getHeure() + ":0" + getMinutes();
        }
        if (getMois() >= 10 && getJour() < 10 && getHeure() >= 10 && getMinutes() < 10) {
            return getAnnee() + "-" + getMois() + "-0" + getJour() + " " + getHeure() + ":0" + getMinutes();
        }
        if (getMois() >= 10 && getJour() < 10 && getHeure() >= 10 && getMinutes() >= 10) {
            return getAnnee() + "-" + getMois() + "-0" + getJour() + " " + getHeure() + ":" + getMinutes();
        }
        if (getMois() < 10 && getJour() >= 10 && getHeure() < 10 && getMinutes() < 10) {
            return getAnnee() + "-0" + getMois() + "-" + getJour() + " 0" + getHeure() + ":0" + getMinutes();
        }
        if (getMois() < 10 && getJour() >= 10 && getHeure() >= 10 && getMinutes() < 10) {
            return getAnnee() + "-0" + getMois() + "-" + getJour() + " " + getHeure() + ":0" + getMinutes();
        }
        if (getMois() < 10 && getJour() >= 10 && getHeure() >= 10 && getMinutes() >= 10) {
            return getAnnee() + "-0" + getMois() + "-" + getJour() + " " + getHeure() + ":" + getMinutes();
        }
        if (getMois() < 10 && getJour() < 10 && getHeure() >= 10 && getMinutes() >= 10) {
            return getAnnee() + "-0" + getMois() + "-0" + getJour() + " " + getHeure() + ":" + getMinutes();
        }
        if (getMois() < 10 && getJour() >= 10 && getHeure() < 10 && getMinutes() >= 10) {
            return getAnnee() + "-0" + getMois() + "-" + getJour() + " 0" + getHeure() + ":" + getMinutes();
        }
        if (getMois() >= 10 && getJour() >= 10 && getHeure() < 10 && getMinutes() >= 10) {
            return getAnnee() + "-" + getMois() + "-" + getJour() + " 0" + getHeure() + ":" + getMinutes();
        }
        if (getMois() < 10 && getJour() < 10 && getHeure() >= 10 && getMinutes() < 10) {
            return getAnnee() + "-0" + getMois() + "-0" + getJour() + " " + getHeure() + ":0" + getMinutes();
        }
        if (getMois() < 10 && getJour() < 10 && getHeure() < 10 && getMinutes() >= 10) {
            return getAnnee() + "-0" + getMois() + "-0" + getJour() + " 0" + getHeure() + ":0" + getMinutes();
        }
        return getAnnee() + "-" + getMois() + "-" + getJour() + " " + getHeure() + ":" + getMinutes();

        }
    
    public boolean equals(Object o) {
        if (o instanceof Date) {
            Date d = (Date)o;
            return (getAnnee() == d.getAnnee()) && (getMois() == d.getMois()) && (getJour() == d.getJour())&& (getHeure() == d.getHeure()) && (getMinutes() == d.getMinutes());
            }
        else
            return false;
        }
    
    // precondition : 'o' est une instance de 'Date' :
    public int compareTo(Object o) {
        Date d = (Date)o;
        if (getAnnee() != d.getAnnee())
            return getAnnee() - d.getAnnee();
        // ici on a forcement annee == d.annee :
        if (getMois() != d.getMois())
            return getMois()  - d.getMois();
        // ici on a forcement annee == d.annee et mois == d.mois :
        if (getJour() != d.getJour())
            return getJour()  - d.getJour();
        // ici on a forcement annee == d.annee et mois == d.mois et jour == d.jour:
        if (getHeure() != d.getHeure())
            return getHeure()  - d.getHeure();
        // ici on a forcement annee == d.annee et mois == d.mois et jour == d.jour et heure == d.heure:
        
        return getMinutes() - d.getMinutes();
        
        
        }

    /**
     * @return the jour
     */
    public int getJour() {
        return jour;
    }

    /**
     * @param jour the jour to set
     */
    public void setJour(int jour) {
        this.jour = jour;
    }

    /**
     * @return the mois
     */
    public int getMois() {
        return mois;
    }

    /**
     * @param mois the mois to set
     */
    public void setMois(int mois) {
        this.mois = mois;
    }

    /**
     * @return the annee
     */
    public int getAnnee() {
        return annee;
    }

    /**
     * @param annee the annee to set
     */
    public void setAnnee(int annee) {
        this.annee = annee;
    }

    /**
     * @return the heure
     */
    public int getHeure() {
        return heure;
    }

    /**
     * @param heure the heure to set
     */
    public void setHeure(int heure) {
        this.heure = heure;
    }

    /**
     * @return the minutes
     */
    public int getMinutes() {
        return minutes;
    }

    /**
     * @param minutes the minutes to set
     */
    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }
    
    }
