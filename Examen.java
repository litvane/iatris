/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
public class Examen {
    private int numArchives;
    private String nom;
    private Patient patient;
    private String observationPraticienHospitalier; 
    private TypeExamen type;
    private Date date;
   
    
    public Examen (int numArchives,String nom, Patient patient, String observationPraticienHospitalier, TypeExamen type, Date date) {
    
        this.numArchives=numArchives;
        this.patient=patient;
        this.nom = nom;
        this.observationPraticienHospitalier = observationPraticienHospitalier;
        this.type = type;
        this.date=date;
        }
    
    public String toString() {
        return ", nom : "+ getNom() + " de type : "+ getType().toString() + "," + "Observation réalisée :"+ getObservationPraticienHospitalier() + "le :"+ getDate().toString();
        }
    
    
    
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public Patient getPatient(){
        return this.patient;
    }
 
    public String getObservationPraticienHospitalier() {
        return observationPraticienHospitalier;
    }

    public void setObservationPraticienHospitalier(String observationPraticienHospitalier) {
        this.observationPraticienHospitalier = observationPraticienHospitalier;
    }
    

    public TypeExamen getType() {
        return type;
    }

    public void setType(TypeExamen type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    }