/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
public class PraticienHospitalier extends Utilisateur {

    private String nom;
    private String prenom;
    private Fonction fonction;
    private String telephone;
    private String id;
    private String mdp;

    public PraticienHospitalier(String nom, String prenom, Fonction fonction, String telephone,String id, String mdp) {
        super(nom,prenom,id,mdp); 
        this.nom = nom;
        this.prenom = prenom;
        this.id = id;
        this.mdp = mdp;
        this.fonction=fonction;
        this.telephone = telephone;
     
    }

    public String getId() {
        return id;
    }

    public Fonction getFonction() {
        return fonction;
    }



    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }
    
    public void setPrenom() {
        this.prenom=prenom;
    }

    public void setTelephone() {
        this.telephone=telephone; 
    }
    
    public String getTelephone() {
        return telephone;
    }

    public String getMdp() {
        return mdp;
    }
    
    public void setMdp() {
        this.mdp=mdp; 
    }

    public String toString() {
        return "Praticien Hospitalier " + prenom + " " + nom + "; " + fonction + "; " + telephone;
    }

//Verification que l’objet passe en parametre est bien une instance de Medecin avant de comparer avec le medecin courant
    public boolean equals(Object o) {
        if (o instanceof PraticienHospitalier) {
            PraticienHospitalier p = (PraticienHospitalier) o;
            return nom.equals(p.nom) && prenom.equals(p.prenom);
        } else {
            return false;
        }
    }
}

