/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;
import BD.*;

import interfaces.InterfaceConnexion;
import interfaces.SecretaireMedicale;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author gasam
 */


    
/**
 *
 * @author Elisa
 * 
 * 
 * 
En tant que praticien hospitalier
Je veux pouvoir accéder aux différents services
Dans le but de localiser le patient et de connaître son parcours de soins

 */



//ipp du patient et service dans la table DMA
//afin qu'il soit possible connaître le service actuel (s'il est différent du service d'entrée) du patient, 
//le praticien hospitalier modifie le service du patient dans le DMA dès qu'il rentre dans son service
public class AjouterFicheDeSoins {
    
   
    private String nomPatient;
    private String prenomPatient;
    private Date dateNaissance;
    private int numArchives;
    private String nomPraticien;
    private String prenomPraticien;
    private Date dateExamen;
    private String typeExamen;
    private String typeImagerie;
    private String resultat;
    private String observations;
    //private static final String configurationFile = "BD.properties";
    private String nom;
    private String prenom;
    private String fonction;
    private String login;
    private String mdp;
    

    public AjouterFicheDeSoins(String nomPatient,String prenomPatient,Date dateNaissance, int numArchives, String prenomPraticien,String nomPraticien, Date dateExamen, String typeExamen, String resultat, String observations) {
        
        this.nomPatient = nomPatient;
        this.prenomPatient=prenomPatient;
        this.dateNaissance=dateNaissance;
        this.numArchives=numArchives;
        this.nomPraticien=nomPraticien;
        this.prenomPraticien=prenomPraticien;
        this.dateExamen=dateExamen;
        this.typeExamen=typeExamen;
        this.typeImagerie=typeImagerie;
        this.resultat=resultat;
        this.observations=observations;
        
        
    }

  

    
    public void connexion(int ipp, String service) {
        try {
            //DatabaseAccessProperties dap = new DatabaseAccessProperties();
            String jdbcDriver = "oracle.jdbc.driver.OracleDriver";
            String dbUrl = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
            String username = "samsong";
            String password = "a12cfc98c8";

            Connection con = DriverManager.getConnection(dbUrl, username, password);

            String query = "SELECT numArchives,dateExamen,nom, prenom, dateNaissance, service, typeExamen, typeImagerie, nom_praticien, prenom_praticien, resultat, observations FROM EXAMEN";
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery(query);
            
}
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        System.out.println("La fiche de soins identifiée par le numéro suivant  : " + numArchives);
        System.out.println("a été réalisée le  "+ dateExamen);
        System.out.println("pour le patient "+ nom +""+ prenom + "né le"+ dateNaissance);
        System.out.println("dans le service de"+ service+".");
        System.out.println("Les examens réalisés étaient de type "+ typeExamen);
        System.out.println(" et les imageries réalisées étaient de type "+typeImagerie+".");
        System.out.println("Ils ont été effectués par le Dr"+nomPraticien +""+prenomPraticien+".");
        System.out.println("Les résultats sont les suivants : "+resultat+".");
        System.out.println("Les observations sont les suivantes : "+observations+".");
        
    }
}