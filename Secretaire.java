/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
public class Secretaire extends Utilisateur {
   
    private String nom;
    private String prenom;
    private String mdp;
    private String acces;
    private String id; 
    
    
    public Secretaire (String nom, String prenom, String id, String mdp) {
        super(nom,prenom,id,mdp); 
        this.nom = nom;
        this.prenom = prenom;
        this.id = id;
        this.mdp = mdp;
        this.acces="ACCES INCONNU";// L’acces est précisé dans les classes SecretaireMedical et SecretaireAdministration
        
        }
    
    public String toString() {
        return "Secrétaire " + prenom + " " + nom ;
        }
    
    public String getAcces() { return acces; }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp (String mdp) {
        this.mdp = mdp;
    }
    
    public String getId(){ 
        return id; 
    }
}

