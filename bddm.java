/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import iatris.DM;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.*;
import iatris.*;
import interfaces.*;
/**
 *
 * @author gasam
 */
public class bddm {

    private Connection con;

    public void insererDM(String service, String dateCreation, String idCreateur, String nom_patient, String prenom_patient, 
            String date_naissance, String raisonAdmission, String resultat, String observationsPH, String prescriptionsPH, 
            String operationsIFSI, String natureMedicoTech, String resultatsMedicoTech, String nom_praticien_responsable, 
            String lettrePHrespo) throws SQLException, ClassNotFoundException {
       
        Statement st = null;
        try {
            //String jdbcDriver = "oracle.jdbc.driver.OracleDriver";
            //String dbUrl = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
            //String username = "samsong";
            //String password = "a12cfc98c8";

            //Connection con = DriverManager.getConnection(dbUrl, username, password);
            
            Class.forName("com.mysql.jdbc.Driver");
            String lien = "jdbc:mysql://mysql-iatris.alwaysdata.net/iatris_db" ;
            Connection con = DriverManager.getConnection(lien, "iatris", "esperansA2021@");
            System.out.println("Connected");
            
            String query = "insert into DM(service, date_creation, idCreateur, nom_patient, prenom_patient, dateNaissance, raisonAdmission, resultat, observationsPH, prescriptionsPH, operationsIFSI, natureMedicoTech, resultatsMedicoTech, nom_praticien_responsable, lettrePHrespo) "
                    + "values('"+service+"','"+dateCreation+"','"+idCreateur+"','"+nom_patient+"','"+prenom_patient+"','"+date_naissance+"','"+raisonAdmission+"','"+resultat+"','"+observationsPH+"','"+prescriptionsPH+"','"+operationsIFSI+"','"+natureMedicoTech+"','"+resultatsMedicoTech+"','"+nom_praticien_responsable+"','"+lettrePHrespo+"')";
            Statement stm = con.createStatement();
            int res = stm.executeUpdate(query);
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    
    public void accesDM(int ipp, String service) throws ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String lien = "jdbc:mysql://mysql-iatris.alwaysdata.net/iatris_db" ;
            Connection con = DriverManager.getConnection(lien, "iatris", "esperansA2021@");
            System.out.println("Connected");

            String query = "SELECT nom_patient,prenom_patient,dateNaissance,idFicheDeSoins,idActe, nom_praticien_responsable,dateActe FROM ACTE";
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery(query);
            
}
        catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void insererActe(String nom_patient, String prenom_patient, String dateNaissance, String idFicheDeSoins,String idActe, String nom_praticien_responsable, String dateActe) throws SQLException, ClassNotFoundException  {
        Statement st = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String lien = "jdbc:mysql://mysql-iatris.alwaysdata.net/iatris_db" ;
            Connection con = DriverManager.getConnection(lien, "iatris", "esperansA2021@");
            System.out.println("Connected");
            
            String query = "insert into ACTE(nom_patient,prenom_patient,dateNaissance,idFicheDeSoins,idActe,nom_praticien_responsable,dateActe) "
                    + "values('"+nom_patient+"','"+prenom_patient+"','"+dateNaissance+"','"+idFicheDeSoins+"','"+idActe+"','"+nom_praticien_responsable+"','"+dateActe+"')";
            Statement stm = con.createStatement();
            int res = stm.executeUpdate(query);
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
     public void suppressionDM(String nom_patient, String prenom_patient, String dateNaissance) throws SQLException, ClassNotFoundException {
       
        Statement st = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String lien = "jdbc:mysql://mysql-iatris.alwaysdata.net/iatris_db" ;
            Connection con = DriverManager.getConnection(lien, "iatris", "esperansA2021@");
            System.out.println("Connected");
            
            String query = "DELETE FROM DM WHERE nom_patient='"+nom_patient+"',prenom_patient='"+prenom_patient+"',dateNaissance='"+dateNaissance+"'";
            Statement stm = con.createStatement();
            int res = stm.executeUpdate(query);
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
     
     public void rechercheDM(String nomPatient, String prenomPatient, String date_Naissance) throws ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String lien = "jdbc:mysql://mysql-iatris.alwaysdata.net/iatris_db";
            Connection con = DriverManager.getConnection(lien, "iatris", "esperansA2021@");
            System.out.println("Connected");

            String query = "SELECT COUNT(*) FROM DM";
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery(query);

            int taille = 0;

            if (res.next()) {
                taille = res.getInt("COUNT(*)");
            }

            String data[][] = new String[taille][15];//tableau avec tout ce qu'il y a dans table DM

            query = "SELECT * FROM DM";
            res = stm.executeQuery(query);

            int i = 0;
            while (res.next()) { //remplissage du tableau 
                String service = res.getString("service");
                String date_creation = res.getString("date_creation");
                String idCreateur = res.getString("idCreateur");
                String nom_patient = res.getString("nom_patient");
                String prenom_patient = res.getString("prenom_patient");
                String dateNaissance = res.getString("dateNaissance");
                String raisonAdmission = res.getString("raisonAdmission");
                String resultat = res.getString("resultat");
                String observationsPH = res.getString("observationsPH");
                String prescriptionsPH = res.getString("prescriptionsPH");
                String operationsIFSI = res.getString("operationsIFSI");
                String natureMedicoTech = res.getString("natureMedicoTech");
                String resultatsMedicoTech = res.getString("resultatsMedicoTech");
                String nom_praticien_responsable = res.getString("nom_praticien_responsable");
                String lettrePHrespo = res.getString("lettrePHrespo");
                
                data[i][0] = service;
                data[i][1] = date_creation;
                data[i][2] = idCreateur;
                data[i][3] = nom_patient;
                data[i][4] = prenom_patient;
                data[i][5] = dateNaissance;
                data[i][6] = raisonAdmission;
                data[i][7] = resultat;
                data[i][8] = observationsPH;
                data[i][9] = prescriptionsPH;
                data[i][10] = operationsIFSI;
                data[i][11] = natureMedicoTech;
                data[i][12] = resultatsMedicoTech;
                data[i][13] = nom_praticien_responsable;
                data[i][14] = lettrePHrespo;
                i++;
            }

            int j = 0;
            while (!nomPatient.equals(data[j][3].replaceAll("\\s+", "")) && j < data.length - 1) {
                j++;
            }

            System.out.println("nom_patient : " + nomPatient);
            System.out.println("prenom_patient : " + prenomPatient);
            System.out.println("dateNaissance : " + date_Naissance);

            if (nomPatient.equals(data[j][3].replaceAll("\\s+", ""))) {
                System.out.println("C'est le même nom là non ? " + data[j][3]);
                if (prenomPatient.equals(data[j][4].replaceAll("\\s+", ""))) {
                    System.out.println("Le prénom c'est ça : " + data[j][4]);
                    if (date_Naissance.equals(data[j][5].replaceAll("\\s+", ""))) {
                        System.out.println("La date de naissance c'est ça : " + data[j][5]);

                        //if le DM existe déjà
                        int input1 = JOptionPane.showOptionDialog(null, "Le DM a déjà été créé", "Création DM", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

                    } else {
                        JOptionPane.showOptionDialog(null, "Ce DM n'existe pas", "Création DM", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                        System.out.println("ah, mauvaise date de naissance");
                    }
                } else {
                    JOptionPane.showOptionDialog(null, "Ce DM n'existe pas", "Création DM", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                    System.out.println("ah, mauvais prénom");
                }
            } else {
                JOptionPane.showOptionDialog(null, "Ce DM n'existe pas", "Création DM", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                System.out.println("ah, mauvais nom");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
         

    public void insererExamen(String numArchives, String dateExam, String nom, String prenom, String dateNaissance, String service, String typeExamen, String typeImagerie, String nom_praticien, String prenom_praticien, String resultat, String observations) throws ClassNotFoundException {
        Statement st = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String lien = "jdbc:mysql://mysql-iatris.alwaysdata.net/iatris_db" ;
            Connection con = DriverManager.getConnection(lien, "iatris", "esperansA2021@");
            System.out.println("Connected");
            
            String query = "insert into EXAMEN(numArchives,dateExam,nom,prenom,dateNaissance,service,typeExamen,typeImagerie,nom_praticien,prenom_praticien,resultat,observations) "
                    + "values('"+numArchives+"','"+dateExam+"','"+nom+"','"+prenom+"','"+dateNaissance+"','"+service+"','"+typeExamen+"','"+typeImagerie+"','"+nom_praticien+"','"+prenom_praticien+"','"+resultat+"','"+observations+"')";
            Statement stm = con.createStatement();
            int res = stm.executeUpdate(query);
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
