/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package iatris;

/**
 *
 * @author Elisa
 */
public class SecretaireMedical extends Secretaire {
    private String nom;
    private String prenom;
    private String acces;
    private String mdp;
    private String id; 
    
    public SecretaireMedical (String nom, String prenom, String mdp,String id) {
        super(nom, prenom,mdp,id);
        this.nom = nom;
        this.prenom = prenom;
        this.id = id;
        this.mdp = mdp;
        this.acces="MEDICAL"; // Permet de definir les droits d’acces à l’application 
        }
    
    public String getAcces() { return acces; }
    
    public String toString() {
        return "Secrétaire médical(e) " + prenom + " " + nom + "; " + acces;
        }
    
}